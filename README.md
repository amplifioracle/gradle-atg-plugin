# Gradle ATG Plugin

This plugin provides ATG-specific operations to be performed using the Gradle (http://www.gradle.org/) build tool.

## Installation

Include this module in your gradle buildscript configuration. You can do this via the published maven asset on Amplifi's public Nexus server:

    buildscript {
        repositories {
            maven { // for direct download of ATG Gradle plugin
                url 'https://maven.amplificommerce.com/content/groups/public'
            }
        }
        dependencies {
            classpath 'com.amplifi.gradle:atg-plugin:1.2-SNAPSHOT'
        }
    }

# Dependency Management

This project makes it easier to include ATG module dependencies at compile time. To utilize the automatic dependency injection provided via the module's manifest file, add the following code to your buildscript's project config:

    atgDependencies {
        useModuleDependencies()
    }

Assuming the gradle build script lies at the root of your module (hint: it should), the above code will read your module's META-INF/MANIFEST.MF file, including the entire classpath of any module listed in the ATG-Required section. This is done automatically and recursively for each module in the dependency tree.
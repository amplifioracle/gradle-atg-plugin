package com.amplifi.gradle

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.tasks.Exec
import org.gradle.api.tasks.StopActionException

class ATGPlugin implements Plugin<Project> {

    void apply(Project project) {
        // extend project with module dependencies
        project.extensions.create('atgDependencies', ATGDependencies, project)
    }
}


/**
 * Class for DSL extension.
 */
class ATGDependencies {
    // create a reference for the project we're extending
    final Project project

    ATGDependencies(final Project project) {
        this.project = project
    }

    // adds a list of classpath entries by inspecting the local module's
    // MANIFEST.MF file and recursing through all the Required modules manifests
    void useModuleDependencies() {
        // preserve scope through closures
        def self = this

        // create a container for all classpath entries
        def deps = []

        // helper: do something with the manifest file of a module
        def withModuleManifest = { modulePath, block ->
            def manifestFile = new File("${modulePath}/META-INF/MANIFEST.MF")
            manifestFile.withReader { reader ->
                def manifest = new org.apache.tools.ant.taskdefs.Manifest(reader)
                block(manifest)
            }
        }

        // recursive function to find classpath dependencies from each module
        def findDependencies
        findDependencies = { modulePath ->
            withModuleManifest(modulePath, { manifest ->
                // include this modules classpath
                def classpath = manifest.getMainSection().getAttributeValue("ATG-Class-Path")
                classpath?.split(/\s+/)?.findAll { classpath_entry ->
                    deps << "${modulePath}/${classpath_entry}"
                }

                // include all dependencies of this module recursively
                def required = manifest.getMainSection().getAttributeValue("ATG-Required");
                required?.split(/\s+/)?.findAll {it}?.each { required_module ->
                    findDependencies("${self.project.rootDir}/../${required_module.replace('.', '/')}")
                }
            })
        }

        // begin recursion
        findDependencies(self.project.projectDir)

        // add dependencies to project
        self.project.dependencies {
            compile self.project.files(deps)
        }
    }
}
